package tasks;

import io.Presentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;;
import java.util.stream.Collectors;

public class Task6 {

    public Task6(ArrayList<MonitoredData> listMonitorData){

        List<String> countPerActivity =
                        listMonitorData.stream()

                .collect(
                Collectors.groupingBy(MonitoredData::getActivity,
                        Collectors.mapping(MonitoredData::getAtivityInterval,
                                Collectors.averagingDouble(interval -> interval.compareTo(5) == -1 ? 1 : 0))))
                        .entrySet().stream().peek(e -> System.out.println(e.toString()))
                                .filter(entry -> entry.getValue() > 0.9)
                                .map(Map.Entry::getKey).collect(Collectors.toList());

        System.out.print(countPerActivity.toString());
        Presentation presentation = new Presentation(6);
        presentation.writeStringToFile("Task6 result: " + countPerActivity.toString());

    }
}
