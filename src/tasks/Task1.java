package tasks;

import io.Presentation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Task1 {
    private ArrayList<MonitoredData> listMonitorData;
    public Task1(){
        String fileName = "Activities.txt";
        listMonitorData = new ArrayList<>();

        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.map(line -> line.split("\t\t"))
                    .map((line) -> new MonitoredData(line[0], line[1], line[2]))
                    .forEach(monitordata -> listMonitorData.add(monitordata));

        } catch (IOException e) {
            e.printStackTrace();
        }

        Presentation presentation  = new Presentation(1);
        presentation.writeListToFile(listMonitorData);

    }

    public ArrayList<MonitoredData> getlistMonitorData(){
        return this.listMonitorData;
    }


    public static void main(String args[]) {

        Task1 task1 = new Task1();
        new Task2(task1.getlistMonitorData());
        new Task3(task1.getlistMonitorData());
        new Task4(task1.getlistMonitorData());
        new Task5(task1.getlistMonitorData());
        new Task6(task1.getlistMonitorData());
    }
}
