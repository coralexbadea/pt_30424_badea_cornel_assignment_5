package tasks;

import io.Presentation;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task2 {

    public Task2(ArrayList<MonitoredData> listMonitorData){

        long sum = Stream.concat(listMonitorData.stream().map(MonitoredData::getStartDay),
                listMonitorData.stream().map(MonitoredData::getEndDay))
                .distinct()
                .collect(Collectors.counting());
        Presentation presentation = new Presentation(2);
        presentation.writeStringToFile("Task2 result: " + sum);
    }
}
