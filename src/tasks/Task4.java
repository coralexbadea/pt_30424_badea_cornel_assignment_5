package tasks;

import io.Presentation;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

public class Task4 {
    public Task4(ArrayList<MonitoredData> listMonitorData){
        Map<Integer, Map<String, Integer>> countPerActivity = listMonitorData.stream()
                .collect(Collectors.groupingBy(MonitoredData::getID,
                        Collectors.groupingBy(MonitoredData::getActivity,Collectors.summingInt(x->1))));

        System.out.print(countPerActivity.toString());
        Presentation presentation = new Presentation(4);
        presentation.writeStringToFile("Task4 result: " + countPerActivity.toString());

    }
}
