package tasks;

import io.Presentation;
import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;


public class Task3 {
    public Task3(ArrayList<MonitoredData> listMonitorData){

        Map<String, Integer> countPerActivity = listMonitorData.stream()
               .collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingInt(x->1)));

        System.out.print(countPerActivity.toString());
        Presentation presentation = new Presentation(3);
        presentation.writeStringToFile("Task3 result: " + countPerActivity.toString());

    }
}
