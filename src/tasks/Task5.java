package tasks;

import io.Presentation;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

public class Task5 {
    public Task5(ArrayList<MonitoredData> listMonitorData){

        Map<String, Integer> countPerActivity = listMonitorData.stream()
                .collect(Collectors.groupingBy(MonitoredData::getActivity,
                        Collectors.summingInt(MonitoredData::getAtivityInterval)));

        System.out.print(countPerActivity.toString());
        Presentation presentation = new Presentation(5);
        presentation.writeStringToFile("Task5 result: " + countPerActivity.toString());

    }
}
