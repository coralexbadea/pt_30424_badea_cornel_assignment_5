package tasks;

import io.Printable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * MonitoredData - this class is used to astractize a tupple composed
 * of the start time of and activity, the end time, and the name of the activty
 */
public class MonitoredData implements Printable{
    private Date startTime;//the time when the activity starts
    private Date endTime;
    private String activity;
    private  SimpleDateFormat format;
    private  static int count=1;//used for computing unique ID for every Object
    private int ID;
    public MonitoredData(String startString, String endString, String activity)
    {
        this.format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        this.activity = activity;
        try {
            this.startTime = format.parse(startString);
            this.endTime = format.parse(endString);
        }catch (ParseException e) {
            e.printStackTrace();
        }
        if(!getStartDay().split("-")[2].equals(getEndDay().split("-")[2])) {
            count++;
        }
        this.ID = count;

    }

    public String getStartDay() {
        return format.format(startTime).split(" ")[0];
    }

    public String getEndDay() {
        return format.format(endTime).split(" ")[0];
    }


    public String getActivity(){
        return this.activity;
    }

    public int getID(){return ID;}

    /**
     * @return - the duration in minutes for an activity
     */
    public Integer getAtivityInterval(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startDateTime = LocalDateTime.parse(format.format(startTime), formatter);
        LocalDateTime endDateTime = LocalDateTime.parse(format.format(endTime), formatter);
        Duration duration = Duration.between(startDateTime, endDateTime);
        Integer minutes = new Integer((int)duration.toMinutes());
        return minutes;
    }
    @Override
    public String print() {
        return ("begin: "+format.format(startTime)+ " end: " + format.format(endTime)+ " activity: " + activity);

    }


}
