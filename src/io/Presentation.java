package io;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
/**
 * Presentation class - this class is used to write to the output files
 */

public class Presentation {
    private String fileName;

    public Presentation(int taskNumber){
        try { //create a file
            this.fileName = "Task_"+ taskNumber +".txt"; //the name of the file
            File myObj = new File(fileName);
            myObj.createNewFile();

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }


    }

    /**
     * writeListToFile method - method to write a list of Objects that extends
     * the Printable interfacec to the output file
     * @param list - list of Objects that extends the Printable
     */
    public void writeListToFile(ArrayList<? extends Printable> list){

        try {
            Files.write(Paths.get(this.fileName),
                    (Iterable<String>) list.stream().map(Printable::print)::iterator);// convert the stream into an Iterable Object
        } catch (IOException e) {
            e.printStackTrace();
            System.out.print("Error while printing to file" + fileName);
        }

    }

    /**
     * @param string - string to be written to the output file
     */
    public void writeStringToFile(String string){
        try {
            FileWriter myWriter = new FileWriter(this.fileName);
            myWriter.write(string) ;
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.print("Error while printing to file" + fileName);
        }

    }
}
