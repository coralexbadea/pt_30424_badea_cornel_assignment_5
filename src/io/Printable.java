package io;

public interface Printable {
    public String print();
}
